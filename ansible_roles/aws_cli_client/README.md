aws_cli_client
=========

An ansible role that sets up a local virtualenv containing awscli and aws-shell.

Requirements
------------

This role is only tested on Centos 7 and Ubuntu 16.04. It requires yum / apt and bash. It expects the users local `.bashrc` to exist and be invoked during runtime.  `PATH` and command completion modifications are made to this file.

This role should be run as a normal local user as it installed into a normal users home directory
and modifies their runtime path.

Role Variables
--------------

`install_dir` : specifies the location to install the aws tools to. This defaults to:
`~/home/bin/aws`

Dependencies
------------

None

Example Playbook
----------------

See `tests/local_aws_client_install.yml`

This can be invoked from the tests directory via `ansible-playbook local_aws_client_install.yml`

License
-------

BSD

