#!/bin/bash - 
#===============================================================================
#
#          FILE: create_admin_key.sh
# 
#         USAGE: ./create_admin_key.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane
#  ORGANIZATION: BCIT
#       CREATED: 13/07/16 15:08
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare key_name="acit_admin"
declare key_type="rsa"
declare file_suffix="_id_${key_type}"
declare key_file="${key_name}${file_suffix}"

ssh-keygen -t "${key_type}" -b 4096 -C "${key_name}" -f "${key_file}" -N ""
cat ${key_file}.pub > authorized_keys

