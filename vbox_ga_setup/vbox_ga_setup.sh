# Installing pre-requisities
yum -y install kernel-devel kernel-headers dkms gcc gcc-c++ kexec-tools

# Creating mount point, mounting, and installing VirtualBox Guest Additions
# Assumes that the virtualbox guest additions CD is in /dev/cdrom
mkdir vbox_cd
mount /dev/cdrom ./vbox_cd
./vbox_cd/VBoxLinuxAdditions.run
umount ./vbox_cd
rmdir ./vbox_cd


