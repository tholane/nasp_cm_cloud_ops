#! /usr/bin/env bash
###################################################################################################
#Preliminary Setup
###################################################################################################
#Flush Tables
iptables -F -t filter
iptables -F -t nat

#Delete all user Defined Chains
iptables -X

###################################################################################################
#NAT Rules 
###################################################################################################

#NAT outbound data from internal network
iptables -t nat -A POSTROUTING                      -s 192.168.1.0/24  -j SNAT --to-source 10.0.255.252

#Port Forward SSH on 52022 to ws2
iptables -t nat -A PREROUTING   -i eth0 -p tcp -d 10.0.255.252 --dport 52022 -j DNAT --to 192.168.1.1:22

#Port Forward SSH on 53022 to ws3
iptables -t nat -A PREROUTING   -i eth0 -p tcp -d 10.0.255.252 --dport 53022 -j DNAT --to 192.168.1.2:22

###################################################################################################
#Filter Input Rules (i.e. for datagrams comming into the router itself)
###################################################################################################

#Policy (i.e. the default)
iptables -t filter -P INPUT DROP

# Inbound Loopback Traffic
#         Table    Action Chain  <- match criteria -> <---- target ---->
#                                 Input Interface      Action 
iptables -t filter -A     INPUT   -i lo                -j ACCEPT

# Inbound SSH Traffic
#         Table    Action Chain  <--------------------------- match criteria -----------------------------> <---- target ---->
#                                Input Interface   Dest IP     Proto   Dest Port   Match Module  state        Action
iptables -t filter   -A   INPUT  -i eth0       -d 10.0.255.252 -p tcp --dport 22   -m state    --state NEW   -j ACCEPT

# Inbound ICMP Traffic
#         Table    Action Chain  <-------------- match criteria ----------> <---- target ---->
#                                Input Interface   Dest IP         Proto       Action
iptables -t filter  -A    INPUT  -i eth0           -d 10.0.255.252 -p icmp    -j ACCEPT 

# Inbound DNS  TCP Traffic
#         Table    Action Chain  <--------------------------- match criteria ------------------------------> <---- target ---->
#                                Input Interface   Dest IP        Proto   Dest Port  Match Module state        Action
iptables -t filter -A     INPUT  -i eth0           -d 10.0.255.252 -p tcp --dport 53 -m state      --state NEW -j ACCEPT

# Inbound DNS  UDP Traffic
#         Table    Action Chain  <-------------------- match criteria --------------> <---- target ---->
#                                Input Interface   Dest IP        Proto   Dest Port    Action
iptables -t filter -A     INPUT  -i eth0          -d 10.0.255.252  -p udp  --dport 53   -j ACCEPT

# Inbound OSPF Traffic
#         Table    Action Chain  <-------------------- match criteria --------------> <---- target ---->
#                                Input Interface  Proto   Action (ospf lsa's are sent to multicast addresses)
iptables -t filter -A     INPUT -i eth0           -p ospf -j ACCEPT

# Established Traffic
#         Table    Action Chain  <------------ match criteria --------------> <---- target ---->
#                                Match Module  State                           Action
iptables -t filter -A     INPUT  -m state      --state ESTABLISHED,RELATED     -j ACCEPT

# Inbound Traffic for Local Network
#         Table    Action Chain  <----- match criteria -------> <---- target ---->
#                                Input Interface  Source IP
iptables -t filter -A     INPUT  -i eth1          -s 192.168.1.0/24 -j ACCEPT

# DHCP Inbound Traffic for Local Network
#         Table    Action Chain  <----- match criteria -----------------------------------------------><---- target ---->
#                                Interface  Source IP   Dest IP            Proto Src Port   Dest Port   Action
iptables -t filter -A     INPUT  -i eth1     -s 0.0.0.0 -d 255.255.255.255 -p udp --sport 68 --dport 67 -j ACCEPT

# Test rule
iptables -t filter -A     INPUT  -i eth0           -d 10.0.255.252 -p tcp --dport 50001 -m state      --state NEW -j ACCEPT

###################################################################################################
#Filter Output Rules (i.e. for datagrams leaving the router itself)
###################################################################################################

#Policy (i.e. the default)
iptables -t filter -P OUTPUT ACCEPT

###################################################################################################
#Filter Forward Rules (i.e. for datagrams being forwarded by the router)
###################################################################################################

#Policy (i.e. the default)
iptables -t filter -P FORWARD DROP

# Allow established traffic through router
#         Table    Action Chain  <------------ match criteria --------------> <---- target ---->
#                                  Match Module  State                         Action
iptables -t filter -A     FORWARD -m state      --state ESTABLISHED,RELATED    -j ACCEPT

# Allow local network traffic through the router
#           Table  Action Chain   <----- match criteria ---------> <---- target ---->
#                                 Input Interface  Source IP        Action
iptables -t filter -A     FORWARD -i eth1          -s 192.168.1.0/24   -j ACCEPT

# Allow ssh traffic to nat hosts
iptables -t filter -A     FORWARD -i eth0  -p tcp  -d 192.168.1.1 --dport 22 -j ACCEPT
iptables -t filter -A     FORWARD -i eth0  -p tcp  -d 192.168.1.2 --dport 22 -j ACCEPT
