# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
PS1='[\u@\h \W]\$ '

##Bash Tracing Propmpt
export PS4=' \[\e[0;34m(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }\e[m\]'

#Set the editor to vim
export EDITOR=/usr/bin/vim

#use vim readline
set -o vi

# the latter is from the EPEL repo
source /usr/share/bash-completion/bash_completion
