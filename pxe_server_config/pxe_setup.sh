# set locale

# Install packages
yum -y install @core @base epel-release vim chrony git kernel-devel kernel-headers dkms gcc gcc-c++ kexec-tools ntp dhcp syslinux-tftpboot tftp-server xinetd nginx tcpdump curl wget nginx

#Install Virtualbox Guest Additions
mkdir vbox_cd
mount /dev/sr0 ./vbox_cd
./vbox_cd/VBoxLinuxAdditions.run
umount ./vbox_cd
rmdir ./vbox_cd

#Sudo Modifications
#Allow all wheel members to sudo all commands without a password by uncommenting line from /etc/sudoers
sed -i 's/^#\s*\(%wheel\s*ALL=(ALL)\s*NOPASSWD:\s*ALL\)/\1/' /etc/sudoers
#Enable sudo over ssh without a terminal
sed -i 's/^\(Defaults    requiretty\)/#\1/' /etc/sudoers

# Disable SELINUX 
setenforce 0
sed -r -i 's/SELINUX=(enforcing|disabled)/SELINUX=permissive/' /etc/selinux/config

#firewall configuration
firewall-cmd --zone=public --add-service=http
firewall-cmd --zone=public --add-service=https
firewall-cmd --zone=public --add-service=tftp

#copy boot image and initrd to tftp directory
cp centos_iso/images/pxeboot/{vmlinuz,initrd.img}  /mnt/sysimage/var/lib/tftpboot/

#copy files from setup dir to final location
cp setup/dhcpd.conf /etc/dhcp/dhcpd.conf
cp setup/tftp /etc/xinetd.d/tftp
cp setup/nginx.conf /etc/nginx/nginx.conf

#copy pxeboot default configuration
mkdir /var/lib/tftpboot/pxelinux.cfg
cp setup/default /var/lib/tftpboot/pxelinux.cfg/default

#copy pxeboot default configuration
mkdir var/lib/tftpboot/pxelinux.cfg
cp setup/default /var/lib/tftpboot/pxelinux.cfg/default

# enable services
systemctl enable sshd ntpd chronyd nginx xinetd dhcpd
