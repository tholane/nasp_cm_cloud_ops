#!/usr/bin/env bash
#===============================================================================
#
#          FILE: create_installer_iso.sh
# 
#         USAGE: ./create_installer_iso.sh 
# 
#   DESCRIPTION: Creates a CentOS installer ISO that incorporates:
#                * custom /EFI/BOOT/grub.cfg for EFI boot that specifies a 
#                  kickstart file
#                * custom /isolinux/isolinux.cfg for BIOS boot that specifies 
#                  a kickstart file
#                * kickstart file
#                * the creation and population of a setup directory on the iso
#                  this directory stores any files used in pre / post installation
#                  scripts within the kickstart installation
#                References:
#                * https://github.com/joyent/mi-centos-7 for similar
#                * https://serverfault.com/questions/517908/how-to-create-a-custom-iso-image-in-centos#521672
#       EXAMPLE: 
#                create_installer_iso.sh" 
#                  -k base_box_int_iso.ks" \
#                  -o CentOS-7-x86_64-Minimal_wp_starting.iso \
#                  -s "wordpress_config_static" \
#                  -m "http://centos.mirror.iweb.ca/7/isos/x86_64/" \
#                  -i "CentOS-7-x86_64-Minimal-1611" \
#                  -d "software_dir"
#             
#       OPTIONS: 
#                -m : mirror site - download location of base iso
#                -i : iso name - the name of the iso to download
#                -o : iso output name - the name of the modified iso
#                -k : kickstart file to embed in iso
#                -s : path of directory that holds setup files to be copied to iso
#                -d : path to software directory that holds cache of downloaded 
#                     and generated iso's
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: * Assumes invoking account has sudo (preferably passwordless)
#                  priveledges to mount the iso as a loopback device
#                * Currently doesn't allow for the inclusion of custom RPM's
#        AUTHOR: Thomas Lane
#  ORGANIZATION: BCIT
#       CREATED: 25/07/16 12:52
#===============================================================================
set -o nounset                              # Treat unset variables as an error

# turn on debugging
set -x

# get the abosolute path of the current script and its directory 
# us this to setup relative paths
declare script_path="$(readlink -f $0)"
declare script_dir=$(dirname "${script_path}")

# Setup location of mounted iso and output directory 
declare iso_mount="${script_dir}/iso_mount"
declare iso_content=${script_dir}/iso_content
declare software_dir="${script_dir}/../software"

# script option handling
OPTIND=1 # reset option index for this script
while getopts :m:i:k:s:d:o: option; do
  case $option in
    m)
      mirror_site="${OPTARG}"
      ;;
    i)
      iso_name="${OPTARG}"
      ;;
    o)
      iso_out="${OPTARG}"
      ;;
    k)
      kickstart_file="${OPTARG}"
      ;;
    s)
      setup_content="${OPTARG}"
      ;;
    d)
      software_dir="${OPTARG}"
      ;;
    *)
      echo "${script_path} ERROR: Invalid option"
      exit 1
      ;;
  esac
done
shift "$((${OPTIND}-1))" # shift off options

# Create a place to store ISO's if you don't have one
if [[ ! -d "${software_dir}" ]] ; then
  mkdir "${software_dir}"
fi

# Download Install ISO if you don't already have it
if [[ ! -f "${software_dir}"/"${iso_name}".iso ]] ; then
  curl "${mirror_site}/sha256sum.txt" --output "${software_dir}/sha256sum.txt"
  curl "${mirror_site}/${iso_name}.iso" --output "${software_dir}/${iso_name}.iso"
  
  pushd "${software_dir}"
  ## Verify download and exit if it isn't ok
  grep "${iso_name}.iso" sha256sum.txt | sha256sum -c
  if [[ $? != 0 ]]  ; then
    echo "sha256sum failed"
    exit 1
  fi
  popd
fi

# Clean up the installation and output iso directories if they prexist
if [[ -d "${iso_mount}" ]] ; then
  rm -rf "${iso_mount}"
fi

mkdir "${iso_mount}"

if [[ -d "${iso_content}" ]] ; then
  rm -rf "${iso_content}"
fi

# Generate writable copy of ISO contents
sudo mount -t iso9660 -o loop,ro "${software_dir}/${iso_name}.iso" "${iso_mount}"
cp -pRf "${iso_mount}" "${iso_content}"

# Cleanup ISO: unmount and delete directory
sudo umount "${iso_mount}"
rm -rf "${iso_mount}"

# Make modifications to ISO content
# Copy the kickstart file to the root of the iso
cp "${kickstart_file}" "${iso_content}/kickstart.ks"

# Copy grub.cfg used to configure boot menu during EFI boot
cp "${script_dir}/iso_updates/grub.cfg" "${iso_content}/EFI/BOOT/grub.cfg"

# Copy isolinux.cfg used to configure boot menu during BIOS boot
cp "${script_dir}/iso_updates/isolinux.cfg" "${iso_content}/isolinux/isolinux.cfg"

# Copy files into Setup directory for use by kickstart file
mkdir "${iso_content}/setup"
cp -r "${setup_content}"/* "${iso_content}/setup/"

# Generate New ISO image
genisoimage -U -r -v -T -J -joliet-long\
  -V "generated_iso"\
  -volset "generated_iso"\
  -A "generated_iso"\
  -b isolinux/isolinux.bin\
  -c isolinux/boot.cat\
  -no-emul-boot\
  -boot-load-size 4\
  -boot-info-table\
  -eltorito-alt-boot\
  -e images/efiboot.img\
  -no-emul-boot\
  -o "${software_dir}"/"${iso_out}" "$iso_content"

# Notes:
# -V volume_id - specifies the volume id (aka name) used by Solaris, MacOS, 
#    Windows this can only be 32 characters long
# -volset volset_id - specifies the volume set name
# -A - string that is written to the volume header, space for 128 characters
# -b eltorito_boot_image - used to provide bootable components on cd image
# -c boot_catalog - needed for bootable cd
# -no-emul-boot - don't attempt to emulate a disk drive
# -boot-load-size 4 - specifieds the number of "virtual" sectors to load
# -boot-info-table specifies the creation of a 56-byte table of CD ROM layout 
#      will be patched in at offeset 8 of the boot file. 
# -eltorito-alt-boot - Start with a new set of El Torito boot parameters
# -e efi_boot_file - EFI boot file name
# -no-emul-boot - specifies that no translations will be performed on boot image
# -o output_file_name - path to output iso
#
# Unused options
# -r - sets the uid, and gid to zero for all files, zeros all write bits, 
#     sets read, and executable bits on all files
# -v - verbose exectution
# -T - generate a translation table  TRANS.TBL for use in certain systems
# -J - generate joliet directory records in addition to ISO file names - useful
#      on Windows machines
# -jolient-long - allow file names up to 103 unicode chagracters.
# -U - allows the use of untranslated file names

# Clean up all the new iso_content
rm -rf $iso_content
