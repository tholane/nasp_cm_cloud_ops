# Cludge to get the path of the directory where the vbox file is stored: creates file adjacent to vbox file
# vboxmanage showvminfo displays line with the path to the config file -> grep "Config file returns it
# the extended regex `(/[^/]+)+' matches everything that is a path i.e. / followed  by anthing not / 

# You will need to change the VM name to match one of your VM's
declare vm_name="test"

declare vm_info="$(vboxmanage showvminfo "${vm_name}")"
declare vm_conf_line="$(echo "${vm_info}" | grep "Config file")"
declare vm_conf_file="$( echo "${vm_conf_line}" | grep -oE '(/[^/]+)+')"
declare vm_directory="$(dirname "${vm_conf_file}")"

echo "${vm_directory}"
