#!/usr/bin/env bash
#===============================================================================
#
#          FILE: get_local_dirl.sh
# 
#         USAGE: get_local_dirl.sh
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: Outputs directory that houses this script. This is a mechanism
#                for creating robust relative paths in your script
#        AUTHOR: Thomas Lane
#  ORGANIZATION: BCIT
#      REVISION: 0.1
#===============================================================================

# get the abosolute path of the current script and its directory 
# us this to setup relative paths
declare script_path="$(readlink -f $0)"
declare script_dir=$(dirname "${script_path}")

echo "The absolute path to ${0} is : ${script_path}"
echo "The parent directory is is : ${script_dir}"


